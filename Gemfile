source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby '2.6.6'

gem 'awesome_print'
gem 'bootsnap', '>= 1.4.2', require: false
gem 'fast_jsonapi'
gem 'has_scope'
gem 'pg', '>= 0.18', '< 2.0'
gem 'puma', '~> 4.1'
gem 'rails', '~> 6.0.3', '>= 6.0.3.2'
gem 'rswag-api'
gem 'rswag-ui'

group :development, :test, :ci do
  gem 'brakeman'
  gem 'bullet'
  gem 'bundler-audit'
  gem 'dotenv-rails'
  gem 'factory_bot_rails'
  gem 'faker'
  gem 'rspec-rails'
  gem 'rswag-specs'
  gem 'rubocop', require: false
end

group :development, :test do
  gem 'byebug'
end

group :development do
  gem 'listen'
  gem 'shog'
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
end

group :test, :ci do
  gem 'database_cleaner', '1.7.0'
  gem 'json_matchers', '~> 0.9.0'
  gem 'shoulda-callback-matchers'
  gem 'shoulda-matchers'
end

group :test do
  gem 'fuubar'
  gem 'simplecov', require: false
end
