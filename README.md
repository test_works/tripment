# README

##### Master

[![pipeline status](https://gitlab.com/test_works/tripment/badges/master/pipeline.svg)](https://gitlab.com/test_works/tripment/-/commits/master)
[![coverage report](https://gitlab.com/test_works/tripment/badges/master/coverage.svg)](https://gitlab.com/test_works/tripment/-/commits/master)

##### Develop

[![pipeline status](https://gitlab.com/test_works/tripment/badges/develop/pipeline.svg)](https://gitlab.com/test_works/tripment/-/commits/develop)
[![coverage report](https://gitlab.com/test_works/tripment/badges/develop/coverage.svg)](https://gitlab.com/test_works/tripment/-/commits/develop)

##

### Project configurations
* Ruby version - **2.6.6**

* Bundler version - **2.1.4**
