module FastJsonApiHelpers
  extend ActiveSupport::Concern

  included do
    def pagination_meta(total_pages)
      PaginationGenerator.new(request: request, total_count: total_pages)
    end
  end
end
