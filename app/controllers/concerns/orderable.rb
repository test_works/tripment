module Orderable
  extend ActiveSupport::Concern

  SORT_ORDER = [:asc, :desc].freeze

  included do
    def apply_order(res, model = nil)
      if params['sort'].present? && params['order'].present?
        res.order(ordering_params(params, model))
      else
        res.recent
      end
    end
  end

  def ordering_params(params, model = nil)
    ordering = {}
    order = params[:order].to_sym
    return unless SORT_ORDER.include?(order)

    attr = params[:sort].underscore
    model ||= controller_name.classify.constantize
    if model.attribute_names.include?(attr)
      ordering[attr] = order
    end
    ordering
  end
end
