module Paginatable
  extend ActiveSupport::Concern

  included do
    def apply_pagination(resource)
      resource.page(params[:page]).per(params[:per_page])
    end
  end
end
