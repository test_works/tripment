class ApplicationController < ActionController::API
  include HasScope
  include FastJsonApiHelpers
end
