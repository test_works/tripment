class ProceduresController < ApplicationController
  has_scope :by_title

  def index
    procedures = apply_scopes(Procedure.all)
    render(
      status: :ok,
      json: ProcedureSerializer.new(procedures).serialized_json
    )
  end
end
