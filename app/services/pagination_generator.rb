class PaginationGenerator
  DEFAULT_PAGE = 1
  DEFAULT_PER_PAGE = 25

  # rubocop:disable Naming/VariableName
  def initialize(request:, total_count:)
    @totalCount = total_count
    @page = request.params[:page]&.to_i || DEFAULT_PAGE
    @perPage = request.params[:per_page]&.to_i || DEFAULT_PER_PAGE
  end
  # rubocop:enable Naming/VariableName
end
