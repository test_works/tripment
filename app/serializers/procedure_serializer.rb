class ProcedureSerializer < BaseSerializer
  attributes :title, :description
end
