class Procedure < ApplicationRecord
  # Callbacks
  before_save :normalize_title, if: :will_save_change_to_title?

  # Scopes
  scope :by_title_starts_with, ->(query) { where('procedures.title ILIKE :query', query: "#{query}%") }
  scope :by_title_contains, ->(query) { where('(procedures.title ~* :first_condition) AND NOT (procedures.title ILIKE :second_condition)', first_condition: query, second_condition: "#{query}%") }
  scope :by_title, ->(query) { (by_title_starts_with(query) + by_title_contains(query)) }

  # Validations
  validates :title, presence: true, uniqueness: true
  validates :description, presence: true

  private

  def normalize_title
    self.title = title.capitalize
  end
end
