if Rails.env.production?
  procedures = [
    {
      title: 'First title',
      description: 'First Description',
    },
    {
      title: 'Second title',
      description: 'Second Description',
    },
    {
      title: 'Third title',
      description: 'Third Description',
    },
    {
      title: 'something first title',
      description: 'something first Description',
    },
    {
      title: 'something second title',
      description: 'something second Description',
    },
    {
      title: 'something third title',
      description: 'something third Description',
    },
    {
      title: 'title first something',
      description: 'title first something',
    },
    {
      title: 'title second something',
      description: 'title second something',
    },
    {
      title: 'title third something',
      description: 'title third something',
    },
    {
      title: 'first something',
      description: 'first something',
    },
    {
      title: 'second something',
      description: 'second something',
    },
    {
      title: 'third something',
      description: 'third something',
    },
  ]

  Procedure.all.destroy_all
  procedures.each { |procedure| Procedure.create(procedure) }
end
