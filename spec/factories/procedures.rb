FactoryBot.define do
  factory :procedure do
    title { Faker::Lorem.words.join(' ') }
    description { Faker::Lorem.sentences.join(' ') }
  end
end
