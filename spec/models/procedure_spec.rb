RSpec.describe Procedure, type: :model do
  it { expect(build(:procedure)).to be_valid }

  let!(:subject) { create(:procedure) }

  describe 'ActiveModel' do
    context 'callbacks' do
      it { expect(subject).to callback(:normalize_title).before(:save).if(:will_save_change_to_title?) }
    end

    context 'scopes' do
      it { expect(described_class).to respond_to(:by_title_starts_with) }
      it { expect(described_class).to respond_to(:by_title_contains) }
      it { expect(described_class).to respond_to(:by_title) }
    end

    context 'validations' do
      it { expect(subject).to validate_presence_of(:title) }
      it { expect(subject).to validate_uniqueness_of(:title) }
      it { expect(subject).to validate_presence_of(:description) }
    end
  end
end
