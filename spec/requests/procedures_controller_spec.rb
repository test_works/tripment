RSpec.describe ProceduresController, type: :request do
  let!(:procedure) { create(:procedure) }

  let(:by_title) { 'test' }

  ######################################################################################################################
  #                                             GET #index
  ######################################################################################################################
  describe 'GET #index' do
    path '/procedures' do
      get 'index' do
        tags 'Procedures'
        consumes 'application/json', 'multipart/form-data'
        produces 'application/json'

        parameter name: :by_title, in: :path, type: :string

        response '200', 'with valid quest' do
          schema '$ref': './spec/support/schemas/procedures.json'
          run_test! do
            expect(response).to match_response_schema('procedures')
          end
        end
      end
    end
  end
end
